// Import NPM packages
const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const replace = require('gulp-replace');
const standard = require('gulp-standard');
const htmllint = require('gulp-htmllint');
const fancyLog = require('fancy-log');
const colors = require('ansi-colors');

// Define file paths
const scssFiles = './scss/**/*.scss';
const jsFiles = './js/**/*.js';

function html() {
    return gulp.src('./*.php')
    .pipe(htmllint({},htmllintReporter));
}

function htmllintReporter(filepath, issues) {
    if (issues.length > 0) {
        issues.forEach(function (issue) {
            fancyLog(colors.cyan('[gulp-htmllint] ') + colors.white(filepath + ' [' + issue.line + ',' + issue.column + ']: ') + colors.red('(' + issue.code + ') ' + issue.msg));
        });
 
        process.exitCode = 1;
    }
}
//compile SCSS into CSS
function style() {
    return gulp.src(scssFiles)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream());
}

// JS files
function js() {
    return gulp.src(jsFiles)
    .pipe(standard())
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js'))
}


function watch() {
    browserSync.init({
        proxy: "http://localhost:8888/mf-front-end-project/"
    });

    gulp.watch(scssFiles, style);
    gulp.watch(jsFiles, js).on('change', browserSync.reload); 
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch('./**/*.html').on('change', browserSync.reload);
    gulp.watch('./dist/css/*.css').on('change', browserSync.reload);
    
}


exports.updateCSS = style;
exports.watch = watch;
exports.js = js;
exports.default = gulp.series(
    gulp.parallel(style, js, html), watch
);

