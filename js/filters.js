
        $(document).ready(function () {

            $('nav#filter').click(function(){
                $('#filter-container').fadeToggle();
            });

            $('.acc').click(function () {
                $('.checkbox-list').hide();
                var id = $(this).attr('id');
                id = id.replace('attr', '');
                $('#checkbox-list' + id).attr('style', ' display:flex !important; width: 100%; flex-wrap: wrap;flex-direction: row;');
                $("a[id^='attr'] > i").removeClass("fa-minus-circle").addClass("fa-plus-circle");
                $(this).find(" > i").removeClass("fa-plus-circle").addClass("fa-minus-circle");                
            });

        });
